<?php

namespace App\Repository;

use App\Entity\Smslog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Smslog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Smslog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Smslog[]    findAll()
 * @method Smslog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SmslogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Smslog::class);
    }

    // /**
    //  * @return Smslog[] Returns an array of Smslog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Smslog
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
