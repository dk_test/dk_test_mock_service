<?php

namespace App\Controller;


use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SmsController extends AbstractController
{
    /**
     * @Route("/sms1", methods={"POST"}, name="sendsms1")
     * @return Response
     */
    function send_sms1(){
        $rnd = rand(1,10000);

        if($rnd > 6000){
            sleep(1);
            throw new Exception('timeout',408);

        }else if($rnd > 3000){
            throw new Exception('error',500);
        }
        return $this->json( 200);

    }

    /**
     * @Route("/sms2", methods={"POST"}, name="sendsms2")
     * @return Response
     * @throws Exception
     */
    function send_sms2(){
        $rnd = rand(1,10000);

        if($rnd > 9500){
            sleep(1);
            throw new Exception('timeout',408);

        }else if($rnd > 8500){
            throw new Exception('error',500);
        }
        return $this->json( 200);


    }


}